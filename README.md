<p align="center">
  <img alt="Psytrance Festivals" src="./src/images/festivals.jpg" width="600" />
</p>
<h1 align="center">
  Psytrance Festivals Around the Globe
</h1>

## Run the project local

1.  **install the basic dependencies**

    - [NodeJS](https://nodejs.org/en/)

2.  **Clone the project and install the dependencies:**

    ```shell
    $ git clone git@gitlab.com:simoneas02/psytrance-festivals.git
    $ cd psytrance-festivals/
    $ npm install

    ```

3.  **Start development mode:**

    ```sh
      $ npm start
    ```

    The site is now running at http://localhost:8000!

4.  **Built with**

    - Gatsby
    - Typescript
    - Saas
    - React
