import React, { ReactNode } from 'react'
import { Link } from 'gatsby'

import * as styles from './layout.module.scss'

type LayoutProps = {
  children: ReactNode
  pageTitle: string
}

const Layout = ({ pageTitle, children }: LayoutProps) => (
  <div className={styles.container}>
    <title>{pageTitle}</title>

    <nav>
      <ul className={styles.navLinks}>
        <li className={styles.navLinkItem}>
          <Link to="/" className={styles.navLinkText}>
            Home
          </Link>
        </li>
        <li className={styles.navLinkItem}>
          <Link to="/about" className={styles.navLinkText}>
            About
          </Link>
        </li>
      </ul>
    </nav>

    <main>
      <h1 className={styles.heading}>{pageTitle}</h1>
      {children}
    </main>
  </div>
)

export default Layout
