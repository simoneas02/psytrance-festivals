import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

import Layout from '../components/layout'

const IndexPage = () => (
  <Layout pageTitle="Psytrance Festivals">
    <p>Psytrance Festivals Around the Globe</p>

    <StaticImage
      alt="PLUR stands for Peace, Love, Unity, and Respect"
      src="../images/plur.jpg"
    />
  </Layout>
)

export default IndexPage
