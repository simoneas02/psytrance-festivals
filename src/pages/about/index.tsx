import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

import Layout from '../../components/layout'

const AboutPage = () => (
  <Layout pageTitle="About">
    <p>
      A collection of festivals around the globe, live the cult of music we are
      people who live and breathe music festivals because Psytrance is more than
      just music that you can occasionally enjoy. It’s rather a way of living.
    </p>

    <StaticImage
      alt="PLUR stands for Peace, Love, Unity, and Respect"
      src="../../images/up.jpg"
    />
  </Layout>
)

export default AboutPage
