import type { GatsbyConfig } from 'gatsby'

const config: GatsbyConfig = {
  siteMetadata: {
    title: `Psytrance`,
    siteUrl: `https://psytrancefestivals.gatsbyjs.io/`,
  },
  plugins: [
    'gatsby-plugin-sass',
    'gatsby-plugin-dts-css-modules',
    'gatsby-plugin-image',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sitemap',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/',
      },
    },
    {
      resolve: 'gatsby-plugin-dts-css-modules',
      options: {
        namedExport: false,

        // banner: '// My own banner',

        customTypings: classes =>
          classes
            .map(className => `export const ${className}: string;`)
            .join('\n'),

        dropEmptyFile: true,
      },
    },
  ],
}

export default config
